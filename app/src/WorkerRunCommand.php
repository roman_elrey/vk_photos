<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 19:53
 */

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once "services/queue/QueueReceiver.php";

class WorkerRunCommand extends Command
{
    protected function configure()
    {
        $this->setName('worker:run')
            ->setDescription('Запуск подписчика. Берет задачу из очереди и сохраняет данные нового пользователя')
            ->setHelp('Worker');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $receiver = new QueueReceiver();

        $receiver->listen();

        $output->writeln('Подписчик запущен');
    }
}