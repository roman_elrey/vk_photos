<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 20:12
 */

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Illuminate\Database\Capsule\Manager as DB;

require_once "config.php";

class UserShowCommand extends Command
{
    protected function configure()
    {
        $this->setName('user:show')
            ->setDescription('Вывод сохраненных пользователей')
            ->setHelp('Пользователи');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        require_once "models/User.php";
        require_once "models/Photo.php";
        require_once "models/Album.php";

        foreach (User::with(['albums', 'photos'])->get() as $user)
        {
            //var_dump($user->photos);
            if ($user->photos && is_object($user->photos)) {
                //echo $user->photos;
            }
        }

        $output->writeln('Сохраненные плозователи:');

        foreach (User::with(['albums', 'photos'])->get() as $user) {
            $output->writeln($user->first_name . ' ' . $user->last_name);

            if ($user->albums && is_object($user->albums)) {
                $output->writeln('Альбомы:');
                foreach ($user->albums as $album) {
                    $output->writeln($album->title);
                }
                $output->writeln(' ');
            }

            if ($user->photos && is_object($user->photos)) {
                $output->writeln('Фотограции:');
                foreach ($user->photos as $photo) {
                    $output->writeln($photo->photo_url);
                }
                $output->writeln(' ');
            }
        }

    }
}