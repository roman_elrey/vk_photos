<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 13:31
 */

require_once __DIR__."/vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'vk_photos',
    'username' => 'root',
    'password' => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
