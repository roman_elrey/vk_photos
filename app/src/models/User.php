<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 13:57
 */

class User extends Illuminate\Database\Eloquent\Model
{
    public function albums() {
        return $this->hasMany(Album::class, 'owner_id', 'user_id');
    }
    public function photos() {
        return $this->hasMany(Photo::class, 'owner_id', 'user_id');
    }
}