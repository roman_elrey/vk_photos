<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 17:14
 */

class Photo extends Illuminate\Database\Eloquent\Model
{
    public function user()
    {
        return $this->belongsTo('User', 'owner_id', 'user_id');
    }
}