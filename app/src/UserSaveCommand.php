<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 13:20
 */

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \GuzzleHttp\Client;

require_once "services/queue/QueueSender.php";

class UserSaveCommand extends Command
{
    protected function configure()
    {
        $this->setName('user:create')
            ->setDescription('Создает задачу сохранения нового пользователя')
            ->setHelp('Создает задачу')
            ->addOption(
                'user_id',
                'u',
                InputOption::VALUE_REQUIRED,
                'Введите идентификатор пользователя',
                null
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user_id = $input->getOption('user_id');

        $this->validateUserId($user_id, $output);

        $simpleSender = new QueueSender();

        $simpleSender->execute($user_id);

        $output->writeln('Задача отправлена в очередь');
    }

    private function validateUserId($user_id, $output)
    {
        if (empty($user_id)) {
            $output->writeln('<error>Пустой идентификатор пользователя</error>');
            exit();
        }
    }
}