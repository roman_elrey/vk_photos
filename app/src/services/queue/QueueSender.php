<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 19:28
 */

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class QueueSender
{

    public function execute($message)
    {
        /**
         * Создаёт соединение с RabbitAMQP
         */
        $connection = new AMQPConnection(
            'localhost',
            5672,
            'guest',
            'guest'
        );


        /** @var $channel AMQPChannel */
        $channel = $connection->channel();

        $channel->queue_declare(
            'vkPhoto',
            false,
            false,
            false,
            false
        );

        $msg = new AMQPMessage($message);

        $channel->basic_publish(
            $msg,       	#message
            '',         	#exchange
            'vkPhoto' 	#routing key
        );

        $channel->close();
        $connection->close();
    }

}