<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 19:46
 */

use PhpAmqpLib\Connection\AMQPConnection;
use \GuzzleHttp\Client;

require_once "services/vk/VkService.php";
require_once "models/User.php";
require_once "models/Photo.php";
require_once "models/Album.php";
require_once "config.php";

class QueueReceiver
{
   public function listen()
    {
        $connection = new AMQPConnection(
            'localhost',	#host
            5672,       	#port
            'guest',    	#user
            'guest'     	#password
        );

        $channel = $connection->channel();

        $channel->queue_declare(
            'vkPhoto',
            false,
            false,
            false,
            false
        );

        $channel->basic_consume(
            'vkPhoto',
            '',
            false,
            true,
            false,
            false,
            array($this, 'processUser')
        );

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * @param $msg
     */
    public function processUser($msg)
    {

        $user_id = intval($msg->body);

        $vkService = new VkService(new Client());

        $userData = $vkService->getUserData($user_id);
        $albumsData = $vkService->getUserAlbums($user_id);
        $photosData = $vkService->getUserPhotos($user_id);

        $user = new User();
        $user->user_id = $user_id;
        $user->first_name = $userData->first_name;
        $user->last_name = $userData->last_name;
        $user->save();

        foreach ($albumsData as $albumData) {
            $album = new Album();
            $album->owner_id = $user_id;
            $album->album_id = $albumData->id;
            $album->title = $albumData->title;
            $album->description = $albumData->description;
            $album->save();
        }

        foreach ($photosData as $photoData) {
            $photo = new Photo();
            $photo->owner_id = $user_id;
            $photo->photo_id = $photoData->id;
            $photo->album_id = $photoData->album_id;
            $photo->photo_url = $photoData->photo_604;
            $photo->save();
        }

        echo 'Сохранены данные пользователя №' . $user->user_id . ' ' . $user->first_name . ' ' . $user->last_name;

    }
}
