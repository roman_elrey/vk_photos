<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 15:19
 */

interface ProviderInterface
{
    /**
     * @param int $user_id
     * @return stdClass
     */
    public function getUserData(int $user_id): stdClass;

    /**
     * @param int $user_id
     * @return array
     */
    public function getUserAlbums(int $user_id): array;

    /**
     * @param int $user_id
     * @return array
     */
    public function getUserPhotos(int $user_id): array;
}