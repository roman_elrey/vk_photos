<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 07.09.18
 * Time: 14:08
 */

require_once "ProviderInterface.php";

class VkService implements ProviderInterface
{
    const HOST = 'https://vk-api-proxy.xtrafrancyz.net/';
    const ACCESS_TOKEN = '48d58aaf48d58aaf48d58aaf454890d98b448d548d58aaf134794d57c032341e26cb3eb';

    private $client;

    /**
     * VkService constructor.
     * @param ClientInterface $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }

    public function getUserData(int $user_id): stdClass
    {
        $url = self::HOST . 'method/users.get?user_id=' . $user_id . '&v=5.52&access_token=' . self::ACCESS_TOKEN . '&fields=first_name';

        $res = $this->client->request('GET', $url);
        if ($res->getStatusCode() === 200) {
            try {
                return json_decode($res->getBody())->response[0];
            } catch (Exception $e) {
                throw new Exception('VK service data parse error');
            }
        }
    }

    public function getUserAlbums(int $user_id): array
    {
        $url = self::HOST . 'method/photos.getAlbums?user_id=' . $user_id . '&v=5.52&access_token=' . self::ACCESS_TOKEN;

        $res = $this->client->request('GET', $url);
        if ($res->getStatusCode() === 200) {
            try {
                return json_decode($res->getBody())->response->items;
            } catch (Exception $e) {
                throw new Exception('VK service data parse error');
            }
        }
    }

    public function getUserPhotos(int $user_id): array
    {
        $url = self::HOST . 'method/photos.get?user_id=' . $user_id . '&v=5.52&access_token=' . self::ACCESS_TOKEN;

        $res = $this->client->request('GET', $url);
        if ($res->getStatusCode() === 200) {
            try {
                return json_decode($res->getBody())->response->items;
            } catch (Exception $e) {
                throw new Exception('VK service data parse error');
            }
        }
    }
}