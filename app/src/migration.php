<?php
require_once "config.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->dropIfExists('users');
Capsule::schema()->create('users', function (Blueprint $table) {
    $table->increments('id');
    $table->string('first_name', 100);
    $table->string('last_name', 100);
    $table->integer('user_id')->unique();
    $table->timestamps();
});

Capsule::schema()->dropIfExists('albums');
Capsule::schema()->create('albums', function (Blueprint $table) {
    $table->increments('id');
    $table->string('title', 100);
    $table->string('description', 100);
    $table->integer('owner_id')->references('user_id')->on('users');
    $table->integer('album_id')->unique();
    $table->timestamps();
});

Capsule::schema()->dropIfExists('photos');
Capsule::schema()->create('photos', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('photo_id')->unique();
    $table->integer('album_id')->references('album_id')->on('albums');
    $table->integer('owner_id')->references('user_id')->on('users');
    $table->string('photo_url', 200);
    $table->timestamps();
});
