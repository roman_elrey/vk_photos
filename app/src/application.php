#!/usr/bin/env php
<?php
// application.php

require __DIR__ . '/vendor/autoload.php';
require 'UserSaveCommand.php';
require 'WorkerRunCommand.php';
require 'UserShowCommand.php';

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new UserSaveCommand());

$application->add(new UserShowCommand());

$application->add(new WorkerRunCommand());

$application->run();